
# Define custom XDG variables
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-${HOME}/.config}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:-${HOME}/.local/cache}
export XDG_DATA_HOME=${XDG_DATA_HOME:-${HOME}/.local/share}
export XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR:-/run/user/$(id -u)}

# Ensures XDG directories exist

mkdir -p "$XDG_CACHE_HOME" "$XDG_CONFIG_HOME" "$XDG_DATA_HOME"

# Define main environment vars
export EDITOR=vim
export VISUAL=vim

# # Timezone to export
# export TZ="America/Toronto"
# #export TZ="Europe/Paris"
# 
# # Define locales
# export LANG=en_US.utf8
# #export LANG=en_US.utf8
# #export LC_ALL=fr_FR.utf8
# #export LC_COLLATE=
# #export LC_CTYPE=
# #export LC_MESSAGES=
# #export LC_MONETARY=
# #export LC_NUMERIC=
# #export LC_TIME=
# #export NLSPATH=

