# cite about-plugin
# about-plugin 'Bash config'

# DOC=This is the base shell configuration
# PRIORITY=10

# Base options

# Correct spelling errors in arguments supplied to cd
shopt -s cdspell

# Adjust text to window size after commands
shopt -s checkwinsize

# Enable globbing for dotfiles
#shopt -s dotglob
# Note: Do not enable this function, this broke std behavior

# Extended globbing
shopt -s extglob

# Append to the history file, don't overwrite it
shopt -s histappend
shopt -s histreedit           # Re-edit failed history substitutions

# Save multi-line commands as one command
#shopt -s cmdhist


# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Completion for hosts
shopt -s hostcomplete

# Record each line as it gets issued
#PROMPT_COMMAND='history -a'



# Bash sane completion !!!

# Perform file completion in a case insensitive fashion
bind "set completion-ignore-case on"

# Display matches for ambiguous patterns at first tab press
bind "set show-all-if-ambiguous on"



