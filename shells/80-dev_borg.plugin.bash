# DOC=Handle backups with borg

#export BORG_PASSPHRASE='monjolimotdepasse'


# Use the repo you created with: borg init -e repokey  xpjez
HM_BORG_REPO=$HOME/data/backups/borg/xpjez
# export BORG_REPO=ssh://username@example.com:2022/~/backup/main

# Directories to backup
HM_BORG_BACKUP_DIRS="$( echo ~/{Documents/,opt/,data/prj/,data/archives/} )"

# Backup retention
HM_BORG_RETENTION="--keep-within=10d --keep-weekly=4 --keep-monthly=-1"



# Commands

hm_backup ()
{
  _hm_backup_ask_pass
}

hm_backup_ls ()
{
  borg list $HM_BORG_REPO
}

hm_backup_run ()
{
  borg create -v --stats  $HM_BORG_REPO::{hostname}_{now:%Y-%m-%d} $HM_BORG_BACKUP_DIRS
}

hm_backup_clean ()
{
  borg prune -v $HM_BORG_RETENTION  $HM_BORG_REPO
}


# Delete: Delete specific backup
# Extract: specific backup to ~/tmp/backups
# Restore: backup to $HOME !!!
# _hm_freespace warn: See https://borgbackup.readthedocs.io/en/stable/quickstart.html

_hm_backup_ask_pass ()
{
  local options=$1

  if ! [[ -z "${BORG_PASSPHRASE-}" ]]; then
    return
  fi

  read -t 600 -sp "ASK: Please enter your Borg password: " answer
  echo ""
  export BORG_PASSPHRASE="${answer}"
  echo "INFO: Password saved in \$BORG_PASSPHRASE "

}


cat <<EOF > /dev/null



DIRECTORIES=~/{Documents/,opt/,data/prj/,data/archives/}

# Create repo (thjis is a one time !, and should not be run in shell ctx)
borg init -e repokey  xpjez

# Create one backup
borg create -v --stats  /home/jez/data/backups/borg/xpjez::{hostname}_{now:%Y-%m-%d} ~/{Documents/,opt/,data/prj/,data/archives/}


# Clean olds backups
borg prune -v --keep-within=10d --keep-weekly=4 --keep-monthly=-1 /home/babar/borg_backup

# List files
borg list $HOME/data/backups/borg/xpjez


EOF


