#!/bin/bash
# DOC=Provide my own aliases toolset
# TAGS=


# -----------------------------------------------------------------------------
# User aliases
# -----------------------------------------------------------------------------

# IDEA
# Faire un script qui catch les programmes root, et il append le prefix avec sudo ...


# SWitch off screens:
# xset dpms force off
# Or better; sleep 1 && xset -display :0.0 dpms force off 
# Src: https://askubuntu.com/questions/62858/turn-off-monitor-using-command-line


# F1-F6
# sudo vbetool dpms off
# : sudo sh -c 'vbetool dpms off; read ans; vbetool dpms on'

# -----------------------------------------------------------------------------
# Testing
# -----------------------------------------------------------------------------




# ssh_key(){
#   if [[ ! -z $1 ]]; then
#     find ~/.ssh -maxdepth 2 -name '*pub' -name "*$1*" | sort | xargs tail -n 999
#   else
#     find ~/.ssh -maxdepth 2 -name '*pub' | sort | xargs tail -n 999
#   fi
# }
# 
# 
# ssh_load_ids() {
# 
#   if [[ ! -z $1 ]]; then
#     pub_keys=$(find ~/.ssh -maxdepth 2 -name '*pub' -name "*$1*" | sort)
#   else
#     pub_keys=$(find ~/.ssh -maxdepth 2 -name '*pub' | sort)
#   fi
# 
#   key_list=""
#   while read -r pub_key
#   do
#     if [[ -f "$(sed 's/\.pub$/.key/' <<< "${pub_key}" )" ]]; then
#       key_list="$key_list $(sed 's/\.pub$/.key/' <<< "${pub_key}" )"
#     elif [[ -f "$(sed 's/\.pub$//' <<< "${pub_key}" )" ]]; then
#       key_list="$key_list $(sed 's/\.pub$//' <<< "${pub_key}" )"
#     fi
#   done <<< "$pub_keys"
# 
#   echo "INFO: Loaded keys"
#   ssh-add -l
#   echo ""
# 
#   echo "INFO: Adding keys:"
#   echo $key_list | tr ' ' '\n'
#   echo ""
#   ssh-add $key_list
# }




alias dfh='df -h'
alias ducwd='du --si | sort -rn'

alias ssync='rsync --rsh=ssh'
alias dl='cd ~/Downloads/'

alias ondemand='sudo cpupower frequency-set -g ondemand'
alias performance='sudo cpupower frequency-set -g performance'

alias nmreconnect='nmcli nm enable false && nmcli nm enable true'

alias jtl='journalctl -b '
alias stl='systemctl'
alias stll='systemctl list-unit-files --state=enabled'
#alias journalctl_clean_6w='du -sh /var/log/journal/; journalctl --vacuum-time=6weeks;  du -sh /var/log/journal/'

alias sdisable=' sudo systemctl disable $@'
alias senable='sudo systemctl enable $@'
alias srestart='sudo systemctl restart $@'
alias sstart='sudo systemctl start $@'
alias sstatus='sudo systemctl status $@'



# This shouild not be here ...
alias y='yadm'



# pwg

alias pwgen='pwgen -1By  16 16'


# Easier port forwarding - usage:
#
# $ portforward project-app1 5432 [5588]
#
# where 'project-app1' is an alias from ~/.ssh/config and 5432 is the remote
# port that you want to forward to. An optional local port can be specified to
# - it omitted, then a random available port will be chosen.
function portforward() {
    if [[ $# -le 1 ]]
    then
        echo "Usage: portforward HOST PORT [LOCALPORT]";
        return
    fi
    HOST=$1
    REMOTE_PORT=$2
    if [[ $# -eq 3 ]]
    then
        LOCAL_PORT=$3
    else
        # Pick a random port and check it is free
        LOCAL_PORT=$((RANDOM+1000))
    fi
    if ! [[ `lsof -i :$LOCAL_PORT | grep COMMAND` ]]
    then
        # Port is free - woop!
        echo "Forwarding to port $REMOTE_PORT on $HOST from http://localhost:$LOCAL_PORT"
        ssh -L $LOCAL_PORT:localhost:$REMOTE_PORT $HOST -N 2> /dev/null
        [[ $? -ne 0 ]] && echo "Unable to connect to $HOST"
    else
        # Recursion ftw
        portforward $HOST $REMOTE_PORT
    fi
}

function _portforward() {
    # Only autocomplete on the first arg
    if [[ ${COMP_WORDS[COMP_CWORD-1]} == "portforward" ]]
    then
        # Extract host aliases from ~/.ssh/config
        HOSTS=$(grep --color=never "^Host " ~/.ssh/config | awk '{if ($2 != "*") print $2}')
        CURRENT_WORD="${COMP_WORDS[COMP_CWORD]}"
        COMPREPLY=($(compgen -W "$HOSTS" -- $CURRENT_WORD))
        return 0
    fi
}

complete -F _portforward portforward


alias sw='randrctl switch-to'

# function power_info ()
# {
#   default                             Enter system default mode
#   rescue                              Enter system rescue mode
#   emergency                           Enter system emergency mode
#   halt                                Shut down and halt the system
#   poweroff                            Shut down and power-off the system
#   reboot [ARG]                        Shut down and reboot the system
#   kexec                               Shut down and reboot the system with kexec
#   exit [EXIT_CODE]                    Request user instance or container exit
#   switch-root ROOT [INIT]             Change to a different root file system
#   suspend                             Suspend the system
#   hibernate                           Hibernate the system
#   hybrid-sleep                        Hibernate and suspend the system
#   suspend-then-hibernate              Suspend the system, wake after a period of
# 
# }





# Usage: ssh_socks_proxy jez@jeznet.org
function ssh_socks_proxy () {
  local host=$1
  local port=9999
  ssh -D $port -N $host
  echo "Listening SOCK5 on 127.0.0.1:$port connected to $host"
}
