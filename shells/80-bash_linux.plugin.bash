# cite about-plugin
# about-plugin 'Bash config for Linux'
# DOC=Configure Linux only bash options

# OSX non-compatibility
if [[ ! "$OSTYPE" =~ darwin* ]]; then

  # Prepend cd to directory names automatically
  shopt -s autocd

  # Correct spelling errors during tab-completion
  shopt -s dirspell

        # Turn on recursive globbing (enables ** to recurse all directories)
  shopt -s globstar
  shopt -s checkjobs

  # Define pager for linux
  export MANPAGER=less
fi

