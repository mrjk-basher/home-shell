

# Start a command in background, redirect output to /dev/null:
s(){
  ("$@" & disown ) >/dev/null 2>&1 </dev/null
  which "$1" >/dev/null 2>&1
}


myips ()
{
  ip a l | grep inet\  | awk '{print $NF, $2}' | sort | column -t
}



lsport() {
  local key=
  case $1 in
    ls|listen)
      key=5
      ;;
    p|process)
      key=7
      ;;
    t|type)
      key=1
      ;;
    *)
      key=7
      ;;
  esac
  ss -luntp | column -t | sort -k $key
}



# Something to show info about something
showme() {
  local name=$1
  env | sort | grep -i $name
  whereis $name
  type $name

}



# Easier port forwarding - usage:
#
# $ portforward project-app1 5432 [5588]
#
# where 'project-app1' is an alias from ~/.ssh/config and 5432 is the remote
# port that you want to forward to. An optional local port can be specified to
# - it omitted, then a random available port will be chosen.
function portforward() {
    if [[ $# -le 1 ]]
    then
        echo "Usage: portforward HOST PORT [LOCALPORT]";
        return
    fi
    HOST=$1
    REMOTE_PORT=$2
    if [[ $# -eq 3 ]]
    then
        LOCAL_PORT=$3
    else
        # Pick a random port and check it is free
        LOCAL_PORT=$((RANDOM+1000))
    fi
    if ! [[ `lsof -i :$LOCAL_PORT | grep COMMAND` ]]
    then
        # Port is free - woop!
        echo "Forwarding to port $REMOTE_PORT on $HOST from http://localhost:$LOCAL_PORT"
        ssh -L $LOCAL_PORT:localhost:$REMOTE_PORT $HOST -N 2> /dev/null
        [[ $? -ne 0 ]] && echo "Unable to connect to $HOST"
    else
        # Recursion ftw
        portforward $HOST $REMOTE_PORT
    fi
}


function _portforward() {
    # Only autocomplete on the first arg
    if [[ ${COMP_WORDS[COMP_CWORD-1]} == "portforward" ]]
    then
        # Extract host aliases from ~/.ssh/config
        HOSTS=$(grep --color=never "^Host " ~/.ssh/config | awk '{if ($2 != "*") print $2}')
        CURRENT_WORD="${COMP_WORDS[COMP_CWORD]}"
        COMPREPLY=($(compgen -W "$HOSTS" -- $CURRENT_WORD))
        return 0
    fi
}

complete -F _portforward portforward


# Usage: ssh_socks_proxy jez@jeznet.org
function ssh_socks_proxy () {
  local host=$1
  local port=9999
  ssh -D $port -N $host
  echo "Listening SOCK5 on 127.0.0.1:$port connected to $host"
}

