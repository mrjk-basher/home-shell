# DOC=Shell Identity Manager


### Default variables
# Define where the user wants the source code to be installed.
# Currently the user must deploy itself idmgr (git clone and done ...)
HM_IDM_ROOT=${HM_IDM_ROOT:-~/.usr/opt/idmgr}

# Define the default id to load. Default: _ (None/Default)
HM_IDM_DEFAULT_ID=${SHELL_ID:-${HM_IDM_DEFAULT_ID:-${HM_PROFILE:-_}}}

# Enable shell debugging when interactive with terminal session
HM_IDM_DEBUG=${HM_IDM_DEBUG:-false}


### Loader
ID_DEBUG=${ID_DEBUG:-${HM_IDM_DEBUG}}
if [ -x $HM_IDM_ROOT/bin/idmgr ]; then
  eval "$( IDM_DIR_ROOT=$HM_IDM_ROOT $HM_IDM_ROOT/bin/idmgr shell)"
fi

# Load default id if available
if idmgr id ls | grep -q "${HM_IDM_DEFAULT_ID}" 2>/dev/null; then
  >&2 echo "INFO: idmgr: Auto loading id '$HM_IDM_DEFAULT_ID' ..."
  idmgr enable $HM_IDM_DEFAULT_ID 2> /dev/null
fi



### Other goodies

load_ubi ()
{
  command -v i &> /dev/null && i q && i ubi
}

load_jez ()
{
  command -v i &> /dev/null && i q && i jez
}

load_mrjk ()
{
  command -v i &> /dev/null && i q && i mrjk
}



