

# Faster Command improvment
alias v='vim '
alias si='sudo -i '

command -v column >/dev/null 2>&1 && alias mountt='mount |column -t '

alias biggest='du --human-readable | sort --human-numeric-sort | tail -n 50'


# Bash aliases
# ============================
alias lspath='echo $PATH | tr ":" "\n"'
reload_bash(){
    . ${HOME}/.bashrc
}

# Unix aliases
# ============================

# Aliases for common chmods
alias 000='chmod 000'   # ----------
alias 600='chmod 600'   # -rw-------
alias 644='chmod 644'   # -rw-r--r--
alias 755='chmod 755'   # -rwxr-xr-x
alias +x='chmod +x '


# ss alignment fix
_ss_wrapper() {
  exec ss $@ | column -t
}
alias ss='_ss_wrapper'


# Iptables
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'


# Misc
alias dfh='df -h'
alias ducwd='du --si | sort -rn'

alias ssync='rsync --rsh=ssh'
alias dl='cd ~/Downloads/'
alias pwgen='pwgen -1By  16 16'



# Distro aliases
# ============================

# Aliases for software managment
#alias pacman='sudo pacman'
#alias update='sudo pacman -Syu'

alias ondemand='sudo cpupower frequency-set -g ondemand'
alias performance='sudo cpupower frequency-set -g performance'

alias nmreconnect='nmcli nm enable false && nmcli nm enable true'

alias jtl='journalctl -b '
alias stl='systemctl'
alias stll='systemctl list-unit-files --state=enabled'
#alias journalctl_clean_6w='du -sh /var/log/journal/; journalctl --vacuum-time=6weeks;  du -sh /var/log/journal/'

alias sdisable=' sudo systemctl disable $@'
alias senable='sudo systemctl enable $@'
alias srestart='sudo systemctl restart $@'
alias sstart='sudo systemctl start $@'
alias sstatus='sudo systemctl status $@'


# Python aliases
# ============================

json2yaml ()
{
  python -c 'import sys, yaml, json; yaml.safe_dump(json.load(sys.stdin), sys.stdout, default_flow_style=False)'
}

yaml2json ()
{
  python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=2)'
}

