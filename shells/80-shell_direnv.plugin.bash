# DOC=Manage direnv



if command -v direnv &>/dev/null ; then
  eval "$(direnv hook bash)"
  >&2 echo "INFO: direnv: is now loaded."
else
  >&2 echo "WARN: direnv: is enabled but not installed."
fi
