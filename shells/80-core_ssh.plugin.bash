# cite about-plugin
# about-plugin 'IDMGR ssh plugin'

# DOC=SSH agent management
# TAGS=sensible


# CONFIG
SSH_AGENT_MAX_TIME=5d
#SSH_AGENT_MAX_TIME=5d


# Agent protection
#ssh_agent_share () {
#  [ "$SSH_CONNECTION" -a "$SSH_TTY" == $(tty) ] && \
#    if [ -z "$SSH_AUTH_SOCK" ]; then
#      # There is no ssh-agent started.
#      # The last connection doesn't come with SSH keys
#      # So we delete all existing key as two people cannot be at the same place
#
#      rm /tmp/ssh-$(whoami) 2>/dev/null
#      find /tmp/ -user $(whoami) -name 'ssh-*' -exec rm -r {} \; 2>/dev/null
#      >&2 echo "SSH Keys: Old keys removed"
#    else
#      # The last connection comes with keys, then we update the symlink with the last key
#
#      # Find all existing sockets
#      EXISTING_SSH_AUTH_SOCK=$(find /tmp/ssh-* -user `whoami` -name agent\* -printf '%T@ %p\n' 2>/dev/null | sort -k 1nr | sed 's/^[^ ]* //' )
#
#      # Delete all other keys, we sure as well to use only the last key version, and I remove the old one
#      OLD_KEYS=0
#      for OLD_KEY in $(echo "${EXISTING_SSH_AUTH_SOCK}" | sed '1 d') ; do
#        rm -r $( dirname $OLD_KEY)
#        OLD_KEYS=$((OLD_KEYS+1))
#      done
#
#      # Do the magic
#      ln -f -s $(echo "${EXISTING_SSH_AUTH_SOCK}" | head -n 1) /tmp/ssh-$(whoami)
#      export SSH_AUTH_SOCK=/tmp/ssh-$(whoami)
#
#      # Show the message the user
#      if [ "${OLD_KEYS}" == "0" ] ; then
#        >&2 echo "SSH Keys: SSH Key loaded"
#      else
#        >&2 echo "SSH Keys: SSH Key loaded, ${OLD_KEYS} Key(s) removed."
#      fi
#    fi
#}
#
## Am I alone with my user ? If yes, uncomment
#shell_ssh_key
#
#
#ssh_agent_clean(){
#
#  # Check if it is a ssh connection
#  [ "$SSH_CONNECTION" -a "$SSH_TTY" == $(tty) ] && \
#    if [ -z "$SSH_AUTH_SOCK" ]; then
#      # Agent is not running, delete all ssh-agent temporary files
#
#      # Delete current environment VARS
#      rm "${XDG_CACHE_HOME}/ssh-agent/env"
#
#      # Delete all existing temporary files
#      find /tmp/ -user $(whoami) -name 'ssh-*' -exec rm -r {} \; 2>/dev/null
#
#      >&2 echo "SSH Keys: Old keys removed"
#    fi
#
#}

ssh_key(){
  if [[ ! -z $1 ]]; then
    find ~/.ssh -maxdepth 2 -name '*pub' -name "*$1*" | sort | xargs tail -n 999
  else
    find ~/.ssh -maxdepth 2 -name '*pub' | sort | xargs tail -n 999
  fi
}


ssh_load_ids() {

  local ssh_profile=${1-}

  # Auto detect profile name
  if [[ -z $ssh_profile ]]; then
    ssh_profile="${SHELL_ID:-${HM_PROFILE-}}"

    if [ -z "$ssh_profile" ]; then
      echo "ERR: You need to provide an ID prefix:"
      find ~/.ssh -maxdepth 2 -name '*pub' | sed -E -e "s@.*/@@" -e "s@_.*@@" | sort | uniq
      return 1
    fi
    >&2 echo "INFO: No id mentionned, using: $ssh_profile"
  fi

  # Look for all keys
  pub_keys=$(find ~/.ssh -maxdepth 2 -name '*pub' -name "*$ssh_profile*" | sort)


  # Export SSH PROFILE ID !!!!
  # SHould not use export but hm_export !
  export SSH_USER_PROFILE=$ssh_profile

  key_list=""
  while read -r pub_key
  do
    if [[ -f "$(sed 's/\.pub$/.key/' <<< "${pub_key}" )" ]]; then
      key_list="$key_list $(sed 's/\.pub$/.key/' <<< "${pub_key}" )"
    elif [[ -f "$(sed 's/\.pub$//' <<< "${pub_key}" )" ]]; then
      key_list="$key_list $(sed 's/\.pub$//' <<< "${pub_key}" )"
    fi
  done <<< "$pub_keys"

  >&2 echo "INFO: ssh: Loaded keys"
  ssh-add -l
  >&2 echo ""

  >&2 echo "INFO: ssh: Adding keys:"
  >&2 echo "$( tr ' ' '\n' <<< "$key_list" | sed 's/^/  /' )"
  >&2 echo ""
  ssh-add $key_list

  >&2 echo "INFO: ssh: Current ID: $SSH_USER_PROFILE (unset SSH_USER_PROFILE to disable)"
}

# Agent startup
ssh_agent_start() {
  #set -x
  local id=${SSH_USER_PROFILE:+profile_$SSH_USER_PROFILE}
  id=${id:-${HM_PROFILE:+profile_$HM_PROFILE}}
  id=${id:-${USER:-default}}

  if [[ -S "${SSH_AUTH_SOCK-}" ]]; then
    # Check if SSH_AUTH_SOCK is available
    >&2 echo "INFO: ssh: Reusing running ssh-agent ($SSH_AGENT_PID:$SSH_AUTH_SOCK) ${SSH_CONNECTION:+via remote connection }"
  # Check if a previous config is avail
  elif [ -f "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" ]; then
    # Yes, it exist ! Let's source it
    . "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" >/dev/null
    >&2 echo "INFO: ssh: Reloading running ssh-agent ($SSH_AGENT_PID:$SSH_AUTH_SOCK) ${SSH_CONNECTION:+via remote connection }"
  else
    mkdir -p "${XDG_RUNTIME_DIR}/ssh-agent/${id}/"
  fi

  # Check current config
  if [[ ! -S "${SSH_AUTH_SOCK}" && -f "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" ]] ; then
    if [ -S "${XDG_RUNTIME_DIR}/ssh-agent/${id}/socket" ]; then
      >&2 echo "WARN: ssh: Cleaning old SSH socket"
      rm "${XDG_RUNTIME_DIR}/ssh-agent/${id}/socket"
    fi
    unset SSH_AUTH_SOCK
    unset SSH_AGENT_PID
    rm "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" 2>/dev/null
    >&2 echo "WARN: ssh: ssh-agent environment cleaned as it was not valid anymore"
  fi

  # Start ssh-agent
  if [[ ! -S "${SSH_AUTH_SOCK}" ]] ; then
    ssh-agent -t $SSH_AGENT_MAX_TIME -a "${XDG_RUNTIME_DIR}/ssh-agent/${id}/socket" -s > "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env"
    . "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" > /dev/null
    >&2 echo "INFO: ssh-agent environment created"
  fi

  # Show present keys
  local rc=0
  local loaded_keys=
  loaded_keys="$(ssh-add -l)"; rc=$?

  if [[ "$rc" -eq 0 ]]; then
    >&2 echo "INFO: ssh: Loaded ssh-keys are:"
    while read -r i; do
      line="$( sed "s@$HOME/.ssh/@@" <<< "$i" | awk '{printf "%-32s%s %s %s", $3, $4, $1, $2}' )"
      >&2 echo "INFO: ssh:  $line"
    done <<< "$(ssh-add -l)"
  else
    >&2 echo "INFO: ssh:  No keys loaded in ssh-keys"
  fi

  #set +x
}




#  # Agent startup
#  ssh_agent_start_old() {
#    set -x
#  
#    # Check if SSH_AUTH_SOCK is available
#    if [[ -S "${SSH_AUTH_SOCK}" ]]; then
#      >&2 echo "INFO: ssh-agent will use started agent ($SSH_AGENT_PID)"
#    # Check if a previous config is avail
#    elif [ -f "${XDG_CACHE_HOME}/ssh-agent/env" ]; then
#      # Yes, it exist ! Let's source it
#      . "${XDG_CACHE_HOME}/ssh-agent/env" >/dev/null
#      >&2 echo "INFO: Reusing previous ssh-agent environment"
#    else
#      mkdir -p "${XDG_CACHE_HOME}/ssh-agent/"
#    fi
#  
#    # Check current config
#    if [[ ! -S "${SSH_AUTH_SOCK}" && -f "${XDG_CACHE_HOME}/ssh-agent/env" ]] ; then
#      unset SSH_AUTH_SOCK
#      unset SSH_AGENT_PID
#      rm "${XDG_CACHE_HOME}/ssh-agent/env" 2>/dev/null
#      >&2 echo "WARN: ssh-agent environment cleaned as it was not valid anymore"
#    fi
#  
#    # Start ssh-agent
#    if [[ ! -S "${SSH_AUTH_SOCK}" ]] ; then
#      ssh-agent -t $SSH_AGENT_MAX_TIME -a "${XDG_CACHE_HOME}/ssh-agent/socket" -s > "${XDG_CACHE_HOME}/ssh-agent/env"
#      . "${XDG_CACHE_HOME}/ssh-agent/env" > /dev/null
#      >&2 echo "INFO: ssh-agent environment created"
#    fi
#  
#    # Show present keys
#    local loaded_keys="$(ssh-add -l)"
#    >&2 echo "INFO: Loaded keys are:"
#    while read -r i; do
#      >&2 echo "INFO: $i"
#    done <<< "$(ssh-add -l)"
#  
#    set +x
#  }

# Logout function
ssh_agent_stop() {

  local id=${SSH_USER_PROFILE:+profile_$SSH_USER_PROFILE}
  id=${id:-${HM_PROFILE:+profile_$HM_PROFILE}}
  id=${id:-${USER:-default}}

  # Disabled while testing
  return 0

  if [ -n "$SSH_AUTH_SOCK" ]; then

    #eval "$(/usr/bin/ssh-agent -k )"
    #rm "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" "${XDG_RUNTIME_DIR}/ssh-agent/${id}/socket"  2> /dev/null

    eval "$(/usr/bin/ssh-agent -k 2>/dev/null)"
    rm "${XDG_RUNTIME_DIR}/ssh-agent/${id}/env" 2> /dev/null
    >&2 echo "INFO: ssh-agent stopped and cleaned"
  fi
}

# Start ssh-agent
ssh_agent_start

# This execute ssh-agent stop on logout
trap 'ssh_agent_stop' 0


# spiette alterative:

#if [[ -z $SSH_AUTH_SOCK ]]; then
#    eval export $(
#        (
#        for b in $(pidof bash);
#        do
#            tr  '\0' '\n' < /proc/$b/environ | grep -w SSH_AUTH_SOCK ;
#        done ) 2>/dev/null |
#                grep SOCK |
#                sort -u |
#                head -n1)
#fi

