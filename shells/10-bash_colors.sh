
# Colors
command -v colordiff >/dev/null 2>&1 && alias diff='colordiff '
if [ -x /usr/bin/dircolors ]; then

  echo YOOOOO

  # Moved in theme, probably not a good idea ...
  #if [ -r "$XDG_CONFIG_HOME/dircolors/dircolors" ] ; then
  #  # Try to load local config
  #  eval "$(dircolors -b "${XDG_CONFIG_HOME}/dircolors/dircolors")"
  #else
  #  # Fall back on system configuration
  #  eval "$(dircolors -b)"
  #fi

  alias ls='ls --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'

  # Note: Is it a good idea to add the --no-messages ?
  alias grep='grep --color=auto --no-messages'
  alias fgrep='fgrep --color=auto --no-messages'
  alias egrep='egrep --color=auto --no-messages'

  alias tree='tree -C'

fi


