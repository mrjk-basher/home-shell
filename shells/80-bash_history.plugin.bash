# cite 'jez_bash_history'
# about-plugin 'Bash history management'

# DOC=Define bash history configuration


HISTFILE="${XDG_DATA_HOME}/bash/bash_history"

# Number of lines kept in history
HISTSIZE=500000

# Number of lines saved in the history after logout
SAVEHIST=100000

# Bash specific options: Use standard ISO 8601 timestamp
HISTTIMEFORMAT='%F %T '

# Avoid duplicate entries
#HISTCONTROL="erasedups:ignoreboth"
HISTCONTROL=""

# Don't record some commands
HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history:clear"



# Prepare detination directory
[ -d "${XDG_DATA_HOME}/bash/" ] || mkdir -p "${XDG_DATA_HOME}/bash/" 2> /dev/null
touch "${HISTFILE}" 2>/dev/null
if [ ! -w "${HISTFILE}" ]; then
  >&2 echo "WARN: Disabling Bash history file (HISTFILE=$HISTFILE)"
  unset HISTFILE
fi


# See: https://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows
