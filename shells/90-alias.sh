

# cd
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../..'
alias cd..='cd ..'

# ls
alias l='ls -1'
alias ls='ls --color=auto -h --group-directories-first'
alias ll='ls -lh'
alias la='ll -Ah'

alias lll='ls -GClFh'
alias ltr='ls -ltr'
alias lsd="ls -Gl | grep ^d"

# mkdir
alias mkdir='mkdir -p'

# others
#alias h='history'
alias j='jobs -l'


# Screen
alias screen='screen -c ${XDG_CONFIG_HOME}/screen/screenrc'

